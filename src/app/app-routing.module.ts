import { NgModule } from '@angular/core'
import { Routes, RouterModule, Route } from '@angular/router'
import { DashboardComponent } from './dashboard/dashboard.component'
import { ChartComponent } from './dashboard/components/smart/chart/chart.component'
import { TableComponent } from './dashboard/components/smart/table/table.component'
import { FarmSelectGuideComponent } from './dashboard/components/smart/farm-select-guide/farm-select-guide.component'

const dashboardRoute: Route = {
  path: 'dashboard',
  component: DashboardComponent,
  children: [
    {
      path: '',
      component: FarmSelectGuideComponent,
    }, 
    {
      path: 'farm/:id/chart',
      component: ChartComponent, 
    },
    {
      path: 'farm/:id/table',
      component: TableComponent,
    },

  ],
}

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  dashboardRoute,
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
