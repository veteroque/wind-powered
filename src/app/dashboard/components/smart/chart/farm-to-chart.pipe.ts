import { Pipe, PipeTransform } from '@angular/core'
import { Farm } from '../../../interfaces/farm.interface'

/**
 * Pipe to Normalize Farm into Ngx-charts data
 *
 * @export
 */
@Pipe({
  name: 'farmToChart',
})
export class FarmToChartPipe implements PipeTransform {

  transform(value: Farm, ...args: any[]): any {

    const { data } = value

    const normalized = data.map(
      item => ({
        name: item.date.toLocaleString('default', { month: 'long' }),
        series: [
          {
            name: 'Produced',
            value: item.produced,
          },
          {
            name: 'Budget',
            value: item.budget,
          },

        ],
      }),
    )

    return normalized
  }

}
