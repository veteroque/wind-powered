import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RouterModule } from '@angular/router'
import { StoreModule } from '@ngrx/store'
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { dashboardReducer } from '../../../store/dashboard.reducer'
import { ChartComponent } from './chart.component'
import { FarmToChartPipe } from './farm-to-chart.pipe'


describe('ChartComponent', () => {
  let component: ChartComponent
  let fixture: ComponentFixture<ChartComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        ChartComponent,
        FarmToChartPipe,
       ],
       imports: [
         NgxChartsModule,
         RouterModule.forRoot([]),
         StoreModule.forRoot({}),
         StoreModule.forFeature('dashboard', {
           ...dashboardReducer,
         }),
       ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
