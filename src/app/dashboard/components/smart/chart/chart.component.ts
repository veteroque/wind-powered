import { Component, ChangeDetectionStrategy } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { State } from '../../../../reducers/index'
import { Farm } from '../../../interfaces/farm.interface'
import { selectFarm } from '../../../store/dashboard.selectors'

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartComponent  {

  constructor( private store: Store<State>) { 

    this.farm$ = store.pipe(select(selectFarm))
  }

  farm$: Observable<Farm>
 
  showXAxis = true
  showYAxis = true
  gradient = false
  showLegend = true
  showXAxisLabel = true
  xAxisLabel = '2019'
  showYAxisLabel = true
  yAxisLabel = 'Power, MWh'
  timeline = true
  colorScheme = {
    domain: ['#9370DB', '#87CEFA', '#FA8072', '#FF7F50', '#90EE90', '#9370DB'],
  }

}