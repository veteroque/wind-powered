import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatTableModule } from '@angular/material/table'
import { RouterModule } from '@angular/router'
import { StoreModule } from '@ngrx/store'
import { dashboardReducer } from '../../../store/dashboard.reducer'
import { TableComponent } from './table.component'


describe('TableComponent', () => {
  let component: TableComponent
  let fixture: ComponentFixture<TableComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        TableComponent,
       ],
      imports: [
        MatTableModule,
        RouterModule.forRoot([]),
        StoreModule.forRoot({}),
        StoreModule.forFeature('dashboard', dashboardReducer),
      ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
