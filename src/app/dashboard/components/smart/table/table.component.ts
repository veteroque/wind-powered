import { Component, ChangeDetectionStrategy } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { State } from '../../../../reducers/index'
import { Farm } from '../../../interfaces/farm.interface'
import { selectFarm } from '../../../store/dashboard.selectors'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {

  constructor( private store: Store<State> ) {
    this.farm$ = store.pipe(select(selectFarm))
  }

   
  farm$: Observable<Farm>

  displayedColumns: string[] = ['month', 'budget', 'released', 'percents']

  getPercentage( budget: number, produced: number): number {
    if (produced == 0 ) return 0
    else return Math.trunc(produced / budget * 100) 
  }

  getPercentageString(budget: number, produced: number): string {

    const val = this.getPercentage(budget, produced)

    if (val === 0 ) {
      return '-'
    } else return val + '%'
  }

  getColor( budget: number, produced: number): 'color-success' | 'color-warn' | '' {
    const value = produced / budget 
    if (produced === 0 ) return ''
    if ( value < .93 )
      return 'color-warn'
      else if (value > 1.07 )
      return 'color-success'
      else return ''
    
  }

  getTotal(farm: Farm) {
    if (!farm) return 0
    const { data } = farm 
    return {
      budget: data.map(t => t.budget).reduce((acc, value) => acc + value, 0),
      produced: data.map(t => t.produced).reduce((acc, value) => acc + value, 0),
      percentage: data.filter(t => t.produced > 0).map(t => this.getPercentage(t.budget, t.produced))
      .reduce((acc, value) => { 
        if ( typeof value === 'number') {
          return acc + value
        } else {
          return acc
        }
        }, 0) / data.filter(t => t.produced > 0).length,
    }
  } 
}
