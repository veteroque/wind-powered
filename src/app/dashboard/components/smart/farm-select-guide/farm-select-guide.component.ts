import { Component } from '@angular/core'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { State } from '../../../../reducers/index'
import { selectFarmsListLoaded } from '../../../store/dashboard.selectors'

@Component({
  selector: 'app-farm-select-guide',
  templateUrl: './farm-select-guide.component.html',
  styleUrls: ['./farm-select-guide.component.scss'],
})
export class FarmSelectGuideComponent { 

  constructor( private store: Store<State>) {
    this.farmsListLoadedSuccess$ = store.pipe(select(selectFarmsListLoaded))
  }
  
  farmsListLoadedSuccess$: Observable<boolean>
  
}
