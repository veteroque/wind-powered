import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { StoreModule } from '@ngrx/store'
import { metaReducers, reducers } from '../../../../reducers/index'
import { FarmSelectGuideComponent } from './farm-select-guide.component'
import { RouterModule } from '@angular/router'
import { dashboardReducer } from '../../../store/dashboard.reducer'

// const storeProvider = StoreModule.forRoot(reducers, {
// 	metaReducers,
// 	runtimeChecks: {
// 		strictStateImmutability: true,
// 		strictActionImmutability: true,
// 	},
// })


describe('FarmSelectGuideComponent', () => {
	let component: FarmSelectGuideComponent
	let fixture: ComponentFixture<FarmSelectGuideComponent>

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				FarmSelectGuideComponent,
			],
			imports: [
				MatProgressSpinnerModule,
				RouterModule.forRoot([]),
				StoreModule.forRoot({}),
				StoreModule.forFeature('dashboard', dashboardReducer),
			],
			providers: [
				// storeProvider,
			],
		})
			.compileComponents()
	}))

	beforeEach(() => {
		fixture = TestBed.createComponent(FarmSelectGuideComponent)
		component = fixture.componentInstance
		fixture.detectChanges()
	})

	it('should create', () => {
		expect(component).toBeTruthy()
	})
})
