import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatButtonModule } from '@angular/material/button'
import { MatSidenavModule } from '@angular/material/sidenav'
import { By } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule } from '@angular/router'
import { SidebarComponent } from './sidebar.component'
import { SidebarLinkButton } from './sidebar.interface'

describe('SidebarComponent test cases', () => {
  let component: SidebarComponent
  let fixture: ComponentFixture<SidebarComponent>
  let buttonsWrapperDe
  let buttonsWrapperEl
  let linkButtons: SidebarLinkButton[]

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarComponent ],
      imports: [
        MatSidenavModule,
        RouterModule.forRoot([]),
        BrowserAnimationsModule,
        MatButtonModule,
        MatSidenavModule,
      ],
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarComponent)
    component = fixture.componentInstance

    buttonsWrapperDe = fixture.debugElement.query(By.css('.mat-drawer-inner-container'))
    buttonsWrapperEl = buttonsWrapperDe.nativeElement

    linkButtons = [{displayName: 'Button', link: 'aaa'}]

    component.opened = true
    component.linkButtons = linkButtons

    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should render links by `linkButtons` input', () => {
    expect(buttonsWrapperEl.children.length).toBe(1, 'rendered buttons count')    
  })




})
