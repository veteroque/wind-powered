export interface SidebarLinkButton {
	displayName: string
	link: string
}