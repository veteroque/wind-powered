import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import { SidebarLinkButton } from './sidebar.interface'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],

  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SidebarComponent {

  @Input()
  opened = false

  @Input() 
  linkButtons: SidebarLinkButton[]

}
