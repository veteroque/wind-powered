
export interface FarmShort {
	id: string
	name: string,
	budget: number
}