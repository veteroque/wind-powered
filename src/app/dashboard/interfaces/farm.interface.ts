import { FarmShort } from './farm-short.interface'

export interface Farm extends FarmShort {
	data: FarmMonthlyData[]	
}

export interface FarmMonthlyData {
	monthNumber: number
	budget: number
	produced?: number
	date: Date | string
}