import { createReducer, on } from '@ngrx/store'
import { FarmShort } from '../interfaces/farm-short.interface'
import { loadFarmsList, loadFarmsListFailure, loadFarmsListSuccess, loadFarm, loadFarmSuccess, loadFarmFailure } from './dashboard.actions'
import { Farm } from '../interfaces/farm.interface'


export interface DashboardState {
	farmsList: FarmShort[]
	farmsListLoading: boolean
	farmsListLoadingSuccess: boolean

	farm: Farm
	farmLoading: boolean
	farmLoadingSuccess: boolean
}

export const initialState: DashboardState = {
	farmsList: [],
	farmsListLoading: false,
	farmsListLoadingSuccess: false,

	farm: undefined,
	farmLoading: false,
	farmLoadingSuccess: false,
}

export const dashboardReducer = createReducer(initialState,

	on( loadFarmsList, state => ({ ...state, farmsListLoading: true }) ),
	
	on( loadFarmsListSuccess, 
		(state, { farms } ) =>  ({ ...state, farmsList: farms, farmsListLoading: false, farmsListLoadingSuccess: true  }), 
	),
	on( loadFarmsListFailure, 
		state => ({ ...state, farmsListLoading: false, farmsListLoadingSuccess: false }),
	),

	
	on( loadFarm, state => ({ ...state, farmLoading: true }) ),
	
	on( loadFarmSuccess, 
		(state, { farm } ) =>  ({ ...state, farm, farmLoading: false, farmLoadingSuccess: true  }), 
	),
	on( loadFarmFailure, 
		state => ({ ...state, farmLoading: false, farmLoadingSuccess: false }),
	),

 )

