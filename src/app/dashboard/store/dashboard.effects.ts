import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { of } from 'rxjs'
import { catchError, exhaustMap, map, mergeMap } from 'rxjs/operators'
import { FarmsService } from '../services/farms.service'
import { FarmsActionTypes, loadFarm, loadFarmsList } from './dashboard.actions'


@Injectable()
export class DashboardEffects {

	loadFarmsList$ = createEffect(() => this.actions$.pipe(
		ofType(loadFarmsList),
		mergeMap(() => this.farms.getFarmsList()
			.pipe(
				map(farms => ({ type: FarmsActionTypes.API_LOAD_FARMS_LIST_SUCCESS, farms })),
				catchError(err => {
					console.warn(err)
					return of({ type: FarmsActionTypes.API_LOAD_FARMS_LIST_FAILURE })
				}),
			)),
	),
	)

	loadFarm$ = createEffect(() => this.actions$.pipe(
		ofType(loadFarm),
		exhaustMap(action => this.farms.getFarm(action.id)
			.pipe(
				map(farm => ({ type: FarmsActionTypes.API_LOAD_FARM_SUCCESS, farm })),
				catchError(err => {
					console.warn(err)
					return of({ type: FarmsActionTypes.API_LOAD_FARM_FAILURE })
				}),
			),
		),
	))

	constructor(
		private actions$: Actions,
		private farms: FarmsService,
	) { }
}