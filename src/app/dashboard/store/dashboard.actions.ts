import { createAction, props } from '@ngrx/store'
import { FarmShort } from '../interfaces/farm-short.interface'
import { Farm } from '../interfaces/farm.interface'


export enum FarmsActionTypes  {
	API_LOAD_FARMS_LIST = '[Farms API] Load farms list',
	API_LOAD_FARMS_LIST_SUCCESS = '[Farms API] Load farms list success',
	API_LOAD_FARMS_LIST_FAILURE = '[Farms API] Load farms list failure',

	
	API_LOAD_FARM = '[Farms API] Load farm',
	API_LOAD_FARM_SUCCESS = '[Farms API] Load farm success',
	API_LOAD_FARM_FAILURE = '[Farms API] Load farm failure',	
}

export const loadFarmsList = createAction(FarmsActionTypes.API_LOAD_FARMS_LIST)

export const loadFarmsListSuccess = createAction(FarmsActionTypes.API_LOAD_FARMS_LIST_SUCCESS,
	props<{farms: FarmShort[]}>(),
)
export const loadFarmsListFailure = createAction(FarmsActionTypes.API_LOAD_FARMS_LIST_FAILURE)



export const loadFarm = createAction(FarmsActionTypes.API_LOAD_FARM,
	props<{id: string}>(),
)
export const loadFarmSuccess = createAction(FarmsActionTypes.API_LOAD_FARM_SUCCESS,
	props<{farm: Farm}>(),
)
export const loadFarmFailure = createAction(FarmsActionTypes.API_LOAD_FARM_FAILURE)