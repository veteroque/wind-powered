import { createSelector } from '@ngrx/store'
import { DashboardState } from './dashboard.reducer'
import { SidebarLinkButton } from '../components/dumb/sidebar/sidebar.interface'


export const selectDashboard = (state) => state.dashboard

export const selectFarmsList = createSelector(
	selectDashboard,
	(state: DashboardState) => state.farmsList,
)

export const selectFarmsListLoading = createSelector(
	selectDashboard,
	(state: DashboardState) => state.farmsListLoading,
)

export const selectFarmsListLoaded = createSelector(
	selectDashboard,
	( state: DashboardState) => state.farmsListLoadingSuccess,
)

export const selectFarm = createSelector(
	selectDashboard,
	(state: DashboardState) => state.farm,
)

export const selectFarmLoading = createSelector(
	selectDashboard,
	(state: DashboardState) => state.farmLoading,
)

export const selectFarmLoaded = createSelector(
	selectDashboard,
	( state: DashboardState) => state.farmLoadingSuccess,
)

export const selectSidebarLinks = createSelector(
	selectDashboard,
	(state: DashboardState): SidebarLinkButton[] => state && state.farm && state.farm.id ? 
		[
			{
				displayName: 'Chart',
				link: `farm/${state.farm.id}/chart`,
			},
			{
				displayName: 'Table',
				link: `farm/${state.farm.id}/table`,
			},
		]
		: [],
)

