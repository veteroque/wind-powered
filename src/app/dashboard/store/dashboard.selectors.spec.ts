import { DashboardState } from './dashboard.reducer'
import * as fromSelectors from './dashboard.selectors'

describe('Dashboard selectors', () => {

	let state: {
		dashboard: DashboardState,
	}

	beforeAll(() => {
		state = { dashboard: { 
			farmsList: [
				{ id: 'NMLf7fLuHX_-RxpdQTSAr', name: 'Alta Wind Energy Center', budget: 450000 }, 
				{ id: 'z4riCbd1swQCimDqi4fxt', name: 'Bethel Wind Farm', budget: 600000 }], 
				farmsListLoading: false, 
				farmsListLoadingSuccess: true, 
				farm: { id: 'z4riCbd1swQCimDqi4fxt', name: 'Bethel Wind Farm', budget: 600000, 
					data: [
				// tslint:disable-next-line
							{ monthNumber: 0, date: '2018-12-31T22:00:00.000Z', budget: 50000, produced: 51755 }, { monthNumber: 1, date: '2019-01-31T22:00:00.000Z', budget: 50000, produced: 58696 }, { monthNumber: 2, date: '2019-02-28T22:00:00.000Z', budget: 50000, produced: 56589 }, { monthNumber: 3, date: '2019-03-31T21:00:00.000Z', budget: 50000, produced: 41405 }, { monthNumber: 4, date: '2019-04-30T21:00:00.000Z', budget: 50000, produced: 47035 }, { monthNumber: 5, date: '2019-05-31T21:00:00.000Z', budget: 50000, produced: 45626 }, { monthNumber: 6, date: '2019-06-30T21:00:00.000Z', budget: 50000, produced: 54994 }, { monthNumber: 7, date: '2019-07-31T21:00:00.000Z', budget: 50000, produced: 0 }, { monthNumber: 8, date: '2019-08-31T21:00:00.000Z', budget: 50000, produced: 0 }, { monthNumber: 9, date: '2019-09-30T21:00:00.000Z', budget: 50000, produced: 0 }, { monthNumber: 10, date: '2019-10-31T22:00:00.000Z', budget: 50000, produced: 0 }, { monthNumber: 11, date: '2019-11-30T22:00:00.000Z', budget: 50000, produced: 0 }
						],
				}, 
				farmLoading: false, 
				farmLoadingSuccess: true } }

	})


	it('should generate sidebar links with current field id', () => {

		const initialState = { farm: { id: 'aaa' } }

		expect(fromSelectors.selectSidebarLinks.projector(initialState))
			.toEqual(
				[{ displayName: 'Chart', link: `farm/aaa/chart` },
				{ displayName: 'Table', link: `farm/aaa/table` }],
			)
	})

	it('should return empty array if current field is undefined', () => {
		const initialState = { farm: undefined }

		expect(fromSelectors.selectSidebarLinks.projector(initialState))
			.toEqual([])
	})

	it('should select farm', () => {
		expect(fromSelectors.selectFarm.projector(state.dashboard))
			.toEqual(state.dashboard.farm)
	})
	
	it('should select farm loading and loadSuccess', () => {
		expect(fromSelectors.selectFarmLoading.projector(state.dashboard))
		.toBe(false, 'farm loading selector')
		
		expect(fromSelectors.selectFarmLoaded.projector(state.dashboard))
		.toBe(true, 'farm loading success selector')
	})
	
	it('should select farms list', () => {
		const state = { farmsList: [{ id: '1', name: 'aaa', budget: 111 }] }
		expect(fromSelectors.selectFarmsList.projector(state))
			.toEqual(state.farmsList)
	})


	it('should select farms list loading and loadSuccess', () => {
		expect(fromSelectors.selectFarmsListLoading.projector(state.dashboard))
			.toBe(false, 'farms list loading selector')

		expect(fromSelectors.selectFarmsListLoaded.projector(state.dashboard))
			.toBe(true, 'farms list loading success selector')
	})
})