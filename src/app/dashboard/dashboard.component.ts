import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'
import { MatSelectChange } from '@angular/material/select'
import { select, Store } from '@ngrx/store'
import { Observable } from 'rxjs'
import { baseUrl } from '../../environments/environment'
import { State } from '../reducers/index'
import { SidebarLinkButton } from './components/dumb/sidebar/sidebar.interface'
import { FarmShort } from './interfaces/farm-short.interface'
import { Farm } from './interfaces/farm.interface'
import { loadFarm, loadFarmsList } from './store/dashboard.actions'
// tslint:ignore-next-line
import {
  selectFarm, selectFarmLoaded, selectFarmLoading,
  selectFarmsList, selectFarmsListLoading, selectSidebarLinks,
} from './store/dashboard.selectors'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class DashboardComponent implements OnInit {

  constructor(
    private store: Store<State>,
  ) {
    this.baseUrl = baseUrl
    this.farmsList$ = store.pipe(select(selectFarmsList))
    this.farmsListLoading$ = store.pipe(select(selectFarmsListLoading))

    this.farm$ = store.pipe(select(selectFarm))
    this.farmLoading$ = store.pipe(select(selectFarmLoading))
    this.farmLoadedSuccess$ = store.pipe(select(selectFarmLoaded))
    this.sidebarLinks$ = store.pipe(select(selectSidebarLinks))
  }

  baseUrl: string
  farm$: Observable<Farm>
  farmLoading$: Observable<boolean>
  farmLoadedSuccess$: Observable<boolean>

  farmsList$: Observable<FarmShort[]>
  farmsListLoading$: Observable<boolean>

  selectedFarmId: string
  sidebarLinks$: Observable<SidebarLinkButton[]>


  ngOnInit() {
    this.store.dispatch(loadFarmsList())
  }

  loadFarmsHandler() {
    this.store.dispatch(loadFarmsList())
  }
  
  farmSelectHandler(evt: MatSelectChange) {

    this.selectedFarmId = evt.value
  }

  handleLoadFarm() {
    this.store.dispatch(loadFarm({ id: this.selectedFarmId }))
  }
}
