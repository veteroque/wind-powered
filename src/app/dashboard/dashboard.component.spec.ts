import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MatButtonModule } from '@angular/material/button'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatSelectModule } from '@angular/material/select'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatToolbarModule } from '@angular/material/toolbar'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule } from '@angular/router'
import { Store, StoreModule } from '@ngrx/store'
import { SidebarComponent } from './components/dumb/sidebar/sidebar.component'
import { DashboardComponent } from './dashboard.component'
import { FarmShort } from './interfaces/farm-short.interface'
import { loadFarmsList, loadFarmsListSuccess } from './store/dashboard.actions'
import { dashboardReducer, DashboardState } from './store/dashboard.reducer'


describe('DashboardComponent', () => {
  let component: DashboardComponent
  let fixture: ComponentFixture<DashboardComponent>
  let store: Store<DashboardState>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        DashboardComponent,
        SidebarComponent,
       ],
      imports: [
        MatSelectModule,
        MatButtonModule,
        MatToolbarModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        BrowserAnimationsModule,
        RouterModule.forRoot([]),
        StoreModule.forRoot({}),
        StoreModule.forFeature('dashboard', dashboardReducer),
      ],
    })
    .compileComponents()
    store = TestBed.get(Store)
    spyOn<any>(store, 'dispatch').and.callThrough()

    fixture = TestBed.createComponent(DashboardComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  
    
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should dispatch loadFarmsList on load', () => {
    const action = loadFarmsList()

    expect(store.dispatch).toHaveBeenCalledWith(action)
  })

  it('should contain farmList$ observable', () => {
    const farmsList: FarmShort[] = [ { id: '1', name: 'aaa', budget: 111 } ]
    const action = loadFarmsListSuccess({ farms: farmsList })

    store.dispatch(action)

    component.farmsList$.subscribe(farms => {
      expect(farms.length).toBe(farmsList.length)
    })

  })

})
