import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatIconModule } from '@angular/material/icon'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatSelectModule } from '@angular/material/select'
import { MatSidenavModule } from '@angular/material/sidenav'
import { MatTableModule } from '@angular/material/table'
import { MatToolbarModule } from '@angular/material/toolbar'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule } from '@angular/router'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { NgxChartsModule } from '@swimlane/ngx-charts'
import { SidebarComponent } from './components/dumb/sidebar/sidebar.component'
import { ChartComponent } from './components/smart/chart/chart.component'
import { FarmToChartPipe } from './components/smart/chart/farm-to-chart.pipe'
import { FarmSelectGuideComponent } from './components/smart/farm-select-guide/farm-select-guide.component'
import { TableComponent } from './components/smart/table/table.component'
import { DashboardComponent } from './dashboard.component'
import { FarmsService } from './services/farms.service'
import { DashboardEffects } from './store/dashboard.effects'
import { dashboardReducer } from './store/dashboard.reducer'
import { mockBackendProvider } from './utils/mock-backend.service'

@NgModule({
  declarations: [
    DashboardComponent,
    FarmSelectGuideComponent,
    ChartComponent,
    TableComponent,
    SidebarComponent,
    FarmToChartPipe,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    RouterModule,
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatCardModule,
    NgxChartsModule,
    MatIconModule,
    StoreModule.forFeature('dashboard', dashboardReducer),
    EffectsModule.forFeature([DashboardEffects]),

  ],
  providers: [
    FarmsService,
    mockBackendProvider,
  ],
})
export class DashboardModule { }
