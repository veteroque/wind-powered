// import { TestBed } from '@angular/core/testing'
// import {
// 	HttpClientTestingModule,
// 	HttpTestingController,
// } from '@angular/common/http/testing'
// import { HTTP_INTERCEPTORS } from '@angular/common/http'
// import { FarmsService } from '../dashboard/services/farms.service'
// import { MockBackendInterceptor } from './mock-backend.service'

// describe(`AuthHttpInterceptor`, () => {
// 	let service: FarmsService
// 	let httpMock: HttpTestingController

// 	beforeEach(() => {
// 		TestBed.configureTestingModule({
// 			imports: [HttpClientTestingModule],
// 			providers: [
// 				FarmsService,
// 				{
// 					provide: HTTP_INTERCEPTORS,
// 					useClass: MockBackendInterceptor,
// 					multi: true,
// 				},
// 			],
// 		})

// 		service = TestBed.get(FarmsService)
// 		httpMock = TestBed.get(HttpTestingController)
// 	})

// 	it('should generate farms data', async () => {
// 		const sub = await service.getFarms()
// 		sub.subscribe(response => {
// 			expect(response).toBeTruthy()
// 		})

// 		const httpRequest = httpMock.expectOne(`http://localhost:4200/api/farms`)

// 		expect(httpRequest).toBeTruthy()

// 	})
// })