// import nanoid from 'nanoid'
import { FarmShort } from '../interfaces/farm-short.interface'
import { Farm } from '../interfaces/farm.interface'
import nanoid from 'nanoid'

// Utils
const getRandomArbitary = (min, max) => parseInt(Math.random() * (max - min) + min)
const getRandomMonthlyProduced = produced => getRandomArbitary(produced * .8, produced * 1.2)

/**
 * Helper function for wind farm data generation
 *
 * @param FarmShort farmShort
 * @returns Farm
 */
const getFarm = (farmShort: FarmShort): Farm => {
	const currentDate = new Date()
	
	const monthlyBudget = farmShort.budget / 12
	
	const farmData: Farm = { ...farmShort, data: [] }
	
	for (let m = 0; m < 12; m++) {
		farmData.data = [...farmData.data,
			
			{
				monthNumber: m,
				date: new Date(currentDate.getFullYear(), m, currentDate.getDay()),
				budget: monthlyBudget, // I think we can adjust this later,
				// depends on factors like windy season or not etc.
				produced: (m < currentDate.getMonth()) ? getRandomMonthlyProduced(monthlyBudget) : 0,
			},
			
		]
	}
	return farmData
}

export const farmsList: FarmShort[] = [
	{
		id: nanoid(),
		name: 'Alta Wind Energy Center',
		budget: 450000,
	},
	{
		id: nanoid(),
		name: 'Bethel Wind Farm',
		budget: 600000,
	},
]

export const farmsData: Farm[] = farmsList.map( farmShort => getFarm( farmShort ) )