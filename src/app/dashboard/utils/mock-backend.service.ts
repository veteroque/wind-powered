import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HTTP_INTERCEPTORS } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { delay } from 'rxjs/operators'
import { baseUrl, backendDelay } from '../../../environments/environment'
import { farmsData, farmsList } from './mock-data'


// Declare mocked routes with farms data 
const urls: MockedUrl[] = [
	{
		url: `${baseUrl}/api/farms`,
		body: farmsList,
	},

	...farmsData.map(farm =>
		(
			{
				url: `${baseUrl}/api/farms/${farm.id}`,
				body: farm,
			}
		)),
]

@Injectable()
export class MockBackendInterceptor implements HttpInterceptor {

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		for (const element of urls) {
			const { body, url } = element

			if (request.url === url) {

				return of(new HttpResponse({ status: 200, body }))
					.pipe(
						delay(backendDelay),
					)
			}
		}
		return next.handle(request)
	}

}


export interface MockedUrl {
	url: string
	body: any
}

export const mockBackendProvider = {
	provide: HTTP_INTERCEPTORS,
	useClass: MockBackendInterceptor,
	multi: true,
}