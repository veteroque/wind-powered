import { HttpErrorResponse } from '@angular/common/http'
import { of } from 'rxjs'
import { FarmShort } from '../interfaces/farm-short.interface'
import { FarmsService } from './farms.service'
import { Farm } from '../interfaces/farm.interface'



describe('FarmsService', () => {

  let httpClientSpy: { get: jasmine.Spy }
  let farmsService: FarmsService

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get'])
    farmsService = new FarmsService(<any>httpClientSpy)

  })

  it('should return expected FarmShort[]', () => {
    const expectedFarmsList: FarmShort[] = [
      { name: 'First farm', id: 'aaa', budget: 111 },
      { name: 'Second farm', id: 'bbb', budget: 222 },
    ]

    httpClientSpy.get.and.returnValue(of(expectedFarmsList))

    farmsService.getFarmsList().subscribe(
      data => expect(data).toEqual(expectedFarmsList, 'expected output'),
    )

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call')

  })
  it('should return expected Farm[]', () => {
    const expectedFarm: Farm = 
    { name: 'Second farm', id: 'bbb', budget: 222, data: []   }
    

    httpClientSpy.get.and.returnValue(of(expectedFarm))

    farmsService.getFarm(expectedFarm.id).subscribe(
      data => expect(data).toEqual(expectedFarm, 'expected output'),
    )

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call')

  })

})
