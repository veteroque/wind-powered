import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { baseUrl } from '../../../environments/environment'
import { FarmShort } from '../interfaces/farm-short.interface'
import { Farm } from '../interfaces/farm.interface'



@Injectable({
  providedIn: 'root',
})

/**
 * Service for Farms REST api communication
 *
 * @export FarmsService
 */
export class FarmsService {


  constructor(private http: HttpClient) { }

  public baseUrl: string

  getFarmsList(): Observable<FarmShort[]> {
    return this.http.get<FarmShort[]>(`${baseUrl}/api/farms`, { observe: 'body' })
  }

  getFarm(id: string): Observable<Farm> {
    return this.http.get<Farm>(`${baseUrl}/api/farms/${id}`, { observe: 'body' })
  }

}
