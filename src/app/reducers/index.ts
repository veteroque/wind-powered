import { ActionReducerMap, MetaReducer } from '@ngrx/store'
import { environment } from '../../environments/environment'

export interface State {
	// dashboard?: DashboardState
}

export const reducers: ActionReducerMap<State> = {

}

// TODO: Move dashboard reducer into dashboard feature

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : []
